This document contains only those talks that have been recorded and where I found the recordings. There's certainly a few more lingering around somewhere waiting to be found.

- Open Source Contributor Automation
    - [Open Source Contributor Automation (30min, PyCon SK)](https://youtu.be/9cxhHaLgG5M?t=1h40m20s)
- coala:
    - [Code Analysis for .* (5 min, PyCon SK)](https://youtu.be/9HWerWgrA4k?t=9h11m18s)
    - [coala: Static Code Analysis for All Languages (15 min, Open Suse Conference 2016)](https://www.youtube.com/watch?v=3AO5e0VFn7Q)
    - [coala: Lint and Fix All Code (5 min, EuroPython 2016)](https://youtu.be/LJ2zRHociVw?t=34m2s)
- Growing an Open Source Community:
    - [Growing an Open Source Community (5 min, EuroPython 2016)](https://youtu.be/QWskFM3zHNg?t=13m7s)
    - [Growing an Open Source Community (25 min, GUADEC 2016)](https://www.youtube.com/watch?v=JOQztgBNV6A)
    